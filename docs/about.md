## About

## Authors
Derek King (deking@splunk.com)

## Credits
This app makes use of several other supporting Splunk apps or Visualisations. 
Special thanks to those who have contributed these apps for the good of the community.

* Alert Manager.

* Alert Manager Technology Add-on
* Event Timeline Viz
* Parallel Coordinates Viz
* URL Parser

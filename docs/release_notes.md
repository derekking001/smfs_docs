** Release Notes **
===================

** Enhancements **
------------------

** Whats new in 1.3.4
- Import content directly into the SMFS Content Management directly from Security Essentials native searches.
- Telemetry added to track page usage
- Tooltips reworked in CSS due to bootstrap data-* being filtered in Splunk v8.0 breaking bootstrap tooltips
- Content Management surface (playbook editor) reworked and tidied up
- Semantic HTML improvements to Playbook Editor as I continue to educate myself out of DIVs for everything!

** Whats new in 1.3.3
- Nothing! Simple uptick on Splunkbase to remove extra file that failed Splunkbase checks. I left a dk.txt file lying around...

** Whats new in 1.3.2
- Improved UI experience for setup, including modal window for writing away event types
- All detail tables now have a row expander to drill down to raw (full fidelity) data
- Squashed Bugs

** Whats new in 1.3.1
- Squashed bugs from the all new 1.3.0 release


** Whats new in 1.3.0
The Security monitoring for Splunk app leverages:

- Tested example detection searches 
- Detection playbook lifecycle framework
- Incident Management framework
- Continuous monitoring
- Support for no SPL investigations
- Advanced threat detection using simple statistics 
...all packaged together in a single app. The goal is to guide the consumer to a smoother onboarding, configuration, and usage experience. 

** What's new with the update?

- Dark Mode Support!
- New Setup Wizard
- Guides the user through dependencies, data onboarding, simple asset management, other configuration(s)
- New Advanced Threat Detection Dashboards
- Continuous Monitoring KPIs to show important information
- Detection Playbooks Framework
- Brings clarity and domain expertise to writing, maintaining and investigating playbook detections. 
- Support for MITRE ATT@CKtactics and techniques, and other arbitrary tagging (Kill Chain / Compliance / Control reference)
- Version control for updated playbooks
- Example Threat Detection searches populated into the detection playbooks framework
- Full incident workflow for detection playbooks
- Using the Alert Manager for Splunk App, Security Monitoring extends the Incident posture dashboards to include tactics, techniques and other tagging.

**Known Issues**
----------------

BUG: 1.3.1-13: Alert Manager Incident Posture screen when in Dark Mode, does not expand the table into dark mode colours (as of version 2.2.2). This has been reported to the Alert Manager support address, without response. If I find a css work around i'll implement it, but right now unlikely i'll tackle this one

WAITING 3rd Party

BUG: 1.3.1-12: Alert Manager does not extract the result_id field from alerts index, eventtype=alert_metadata, meaning incident review dashboard does not populate. The workaround is to create a field extraction and make sure its a global extraction

![ScreenShot ](img/result_id.png)

OUTSTANDING

BUG: 1.3.1-7: V8.0 has broken tooltips on playbook editor, and on advanced dashboards....

OUTSTANDING


BUG: 1.3.0,1.3.1-6 - Incorrectly_Routed_DNS_Traffic search uses a field thats not available as an extracted field. (vendor_action=pass) - suspect this should be action='allowed'... 

OUTSTANDING





** Change Log **
----------------
Version: 1.3.4 (17th April 2020)
*	Support for Directly importing Security Essentials Detections into the Content Management Screen
	Playbook Editor Rework
	Page Telemetry
	Tooltips fixed due to 8.0 splunk breaking bootstrap tooltips
	sementic changes to some HTML markup

Version: 1.3.2 (Dec 9th 2019) - Multiple Changes Including
*	Fixed Tab Background Colour 
	All details table now support dynamic in table drill down to the raw event
	Performance enhancements to the searches that run on setup for testing field extractions
	UI tweaks to the setup wizard
	Multiple Bugs Squashed

Version: 1.3.1 (Nov 3rd 2019) - Added support for Splunk version 8.0
*	Fixed Tabs in Continous Monitoring Dashboards

Version: 1.3.0 (Oct 31st 2019) - All new version (see Whats new section for more details)

Version: 1.05 (Dec 18) - Version 1.0.5

*    Fixed a lookup naming clash with windows_ta.

Version: 1.03 (Oct 18) - Bug Fixes

Version: 1.0 (Oct 18) - Initial Release



#Dependancies

Rather than reinventing the wheel Security Monitoring for Splunk works on the premise that there are many many great apps on splunkbase that do a great job, and therefore leverages those rather than reinventing the 'wheel'. As such you should download and install the following apps before configuring Security Monitoring for Splunk.

##Alert Manager App
Incident Workflows are provided by the Alert Manager app (LINK). Follow the setup instructions carefully for this app, in particular ensure you create the 'alerts' index and check the box to write incidents to the Index and KVStore. [Alert Manager Link](https://splunkbase.splunk.com/app/2665/ "Alert Manager")

##Alert Manager TA
This technology add-on is a dependancy for the Alert Manager app. [Alert Manager TA Link](https://splunkbase.splunk.com/app/3365/ "Alert Manager Add-on") 

##Event Timeline Viz
The event timeline visualisation is used to bring context to a number of dashboards. [Event Timeline Viz Link](https://splunkbase.splunk.com/app/4370/ "Event Timeline Viz")

##Parallel Coordinates App
The parallel coords visualisation is used in every Continuous Monitoring dashboard inside the app, to help you visualise relationships between entities specific to the dashboard monitoring area. [Parallel Coordinates Link](https://splunkbase.splunk.com/app/3137/ "Parallel Coordinates")

##URL Parser
URL Parser is simply the 'only' way to deal with URL paths of any kind and much much more. URL parser is required for Advanced Threat Detection dashboards that help you analyse URL/URI strings. [URL Parser Link](https://splunkbase.splunk.com/app/3396/ "URL Parser")

##Horizon Chart
Horizon chart is a visualisation that is used on the System Status dashboard, used to show you incoming data and missing data sources. [Horizon Chart Link](https://splunkbase.splunk.com/app/3117/ "Horizon Chart") 

##Security Essentials
If you want to import content from [Security Essentials](https://splunkbase.splunk.com/app/3435) then you need to have the app installed. 


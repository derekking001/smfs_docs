** Importing Security Essentials Detections**
=============================================
If you have installed the Splunk Security Essentials app, you will be aware there are a lot of detection search ideas that can be implemented into your Splunk environment.
That's what this dashboard is for!

Because Security Essentials is essentially a 'window' into most of the detection searches that are often ran across all of the Splunk security portfolio, we only import into this app those searches
listed as having Live searches. Searches that are from premium products like UBA, Enterprise Security or those listed as Demo are not pulled into Security Monitoring. 

As a result, you can import over 100 detection searches.

*Import Process

You can import searches from two places. Either use the navigation menu (Detection Playbooks -> Import Security Essentials Detections), or from the Playbooks dashboard, click the Import SSE Searches button.

1) Hit the 'Get SSE Searches' button. 

	This should return you a page of detections that you can click on individually to import into playbooks.

2) Apply the event source that the detection search should look at, and select the Report Category

3) Click 'Import'

That's it! 

** Warning - SMFS does not stop you reimporting the same searches multiple times. For a number of reasons, but will tell you before pressing Import. 
In addition, the card displaying the search will be blurred out, and have a green left margin. Although you are allowed to click and re-import if you choose. 

** Operationalise the playbook
Once imported its time to operationalise, and make it effective against your data!

To do this, simply select the playbook in the playbook dashboard, and click on the 'Edit' button in the playbook editor.
From here, you can update all the fields, and run / re run the search until it does what you need. Once it finds the data you need, click 'Save Search' right under the search, 
and then 'Save' top right.




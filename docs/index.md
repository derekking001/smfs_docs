# Welcome to Security Monitoring for Splunk Documentation 

Use the navigation sections to learn how to install, configure, and use the
Security Monitoring app. 

Feel free to contact me with suggestions for improvement and issues
at deking[@]splunk[.]com

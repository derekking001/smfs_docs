**Communications Monitoring Dashboard**
=======================================

The communications dashboard aims to monitor network traffic in your environment. You can click on any visualisation to drill into the raw events. Any table cells highlighlited in red, are fields that have not been extracted. Use the setup dashboard to make any adjustments to you need.



**Firewall Tab**
----------------

The firewall tab is designed to give you information about sending and receiving of network traffic to and from external sources, using what network protocols, and the relationships between them. All firewalls sending traffic information should report the required fields below.

Most security incidents and breaches begin with making a connection through the firewall using ports and protocols that are permitted by default. (HTTP/HTTPs/DNS etc). Knowing who you have been communicating with, how and how much information you have sent or received is a vital piece of understanding your environment and for investigating incidents.


**The dashboards and their typical uses are listed below;**

**Traffic Streams by action:** Look for Spikes or dips to spot any unusual activity based on previous history

**Traffic Stream Relationships:** Look for suspicious relationships between sending and receiving hosts, unusual port activity or identify other compromised hosts using relationships

**Traffic Streams Detail:** Use this panel to understand the specifics of every network communication sent and received over a time span. Click the right facing chevron to see a timeline of all data moved in that session

**Typical Sources of Information**:

*   Syslog either TCP or UDP.

**Required Fields**

|Splunk Field Name|Description|Example|
|--- |--- |--- |
|src_ip|Source IP address|192.168.1.200|
|src_port|Source Port|53202|
|dest_ip|Destination IP address|52.100.100.100|
|dest_port|Destination Port|443|
|action|Action taken by the firewall|"allowed", "blocked"|

Extract more fields for Firewall Events or look at the tutorials page to find out how.


**Email Tab**
-------------

The email tab is designed to give you information about sending and receiving of emails, by whom and to whom, and the relationships between them. Some mail systems will not have all of the fields below, but thats OK. Just extract the information you have.

Most security incidents and breaches begin with a phishing attack to one or more users, and being able to monitor flow of email along with attachment name and file size will greatly speed up your capability to respond effectively to attempted or confirmed attacks.

**The dashboards are their typical uses are listed below;**

**Email Volume by sourcetype:** Look for Spikes or dips to spot any unusual activity based on previous history

**Email Flow Relationships:** Look for suspicious senders, or subjects and understand who else or how many others have also received the same email

**Detailed Email Tracking:** Use this panel to understand the specifics of every email sent and received over a time span

**Typical Sources of Information:**

*   Exchange Debug Logs
*   Email Proxy Filters
*   Splunk Stream

**Required Fields**

|Splunk Field Name|Description|Example|
|--- |--- |--- |
|recipient|Recipient of the Email|fred@wackyraces.com|
|src_user|Sender of the email|wilma@goingmad.com|
|subject|Subject|Your Late Home|
|file_name|Name of file attachment(s) if available|rulez.doc|
|file_size|Size of attachment(s) if available|100|

Extract more fields for Email Events or look at the tutorials page to find out how.


**Web Proxy Tab**
-----------------

Flows of Web traffic are extremely useful to understand usage, trends and categories of web viewing. But more importantly may help to identify how an organisation may have been attacked from compromised external web sites, identifying attackers infrastructure, understanding the tools being employed against us or identifying internal employee behaviour (with bad intent or otherwise).

Filter on time, reporting web proxy, user, http response code, http method and url. This is particularly useful when trying to determined the source of malicious traffic to adversary controlled infrastructure.

**The dashboards are their typical uses are listed below;**

**Web activity by user:** Look for spikes or dips to spot any unusual activity based on previous history

**Web Traffic Relationships:** Look for suspicious relationships between user, uri path and external web site

**Detailed Web Traffic:** Use this panel to understand the specifics of every connection sent and received through the web proxy over a time span

**Typical Sources of Information:**

*   Web Proxies
*   Splunk stream

**Required Fields**

|Splunk Field Name|Description|Example|
|--- |--- |--- |
|src_ip|Source IP|192.168.100.100|
|http_user_agent|User Agent String|Mozilla/5.0 (iPad; U; CPU OS 3_2_1 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Mobile/7B405|
|url|Visited Web Address|www.hackerforums.net|
|user|User visiting the website|fred|
|http_method|Request Method|GET, POST|
|status|Status Code|200|
|http_content_type|Type of content loaded|text/html application/octet|

Extract more fields for Web Proxy Events or look at the tutorials page to find out how.


**DNS Tab**
-----------

DNS query logs are one of, if not the richest source of information when hunting for badness in your network. Communications internal and external mostly rely on DNS to lookup IP addresses of the hosts they need to connect with, making it a single source of truth.

Ingesting this data source provides an organisation with the ability to track user communications, bad actor infrastructure, detect malware that has gone undetected by anti-virus solutions and much more.

**The dashboards are their typical uses are listed below;**

**DNS Traffic by Record Type:** Look for spikes or dips to spot any unusual activity based on previous history

**Detailed DNS Traffic:** Use this panel to understand the specifics of every DNS connection sent and received over a time span

**Typical Sources:**

*   Windows DNS Query logs
*   Bro DNS
*   Splunk Stream

**Required Fields:**

|Splunk Field Name|Description|Example|
|--- |--- |--- |
|src_ip|Source IP|192.168.100.100|
|query|Web Address Queried|www.bbc.co.uk|
|query_type|Request Type|A,AAAA,MX,CNAME|

Extract more fields for DNS Events or look at the tutorials page to find out how.

**DHCP Tab**
------------

DHCP Information provides you with a way to attribute traffic you see in logs that only provide an IP address, with an actual end host, either by NetBios name or by MAC address. Being able to generate this level of attribution is key to being able to build a timeline of events with triaging incidents, but also in other areas such as building or verifying asset inventories, detecting new devices or unused devices.

**Typical Sources of information:**

*   Windows DHCP
*   BIND Logs

**Required Fields:**

|Splunk Field Name|Description|Example|
|--- |--- |--- |
|dest_nt_host|NT Hostname requesting a DHCP address|WIN-HOST1|
|dest_mac|Mac Address of the requesting host|00:50:56:c0:00:01|
|dest_ip|IP address assigned to the client|192.168.200.10|

Extract more fields for DHCP Events or look at the tutorials page to find out how.

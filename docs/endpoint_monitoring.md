**Endpoint Monitoring Dashboard**
=================================

The endpoint monitoring dashboard aims to monitor information typically found on client and server devices. You can click on any visualisation to drill into the raw events. Any table cells highlighted in red are fields that have not been extracted. Use the setup dashboard to make any adjustments you may need fields.

Click on the tabs below to find out more about using each of the user monitoring tabs.

**Malware Tab**
---------------

The malware tab is designed to give you information about what known malicious software is spotted in your network, where is has impacted and what relationships exist.

Malware solutions are a traditional form of defense, and is a good base for spotting broad well known bad files.

Use the "Show and Hide Filters" button to have your dashboard react to information you care about.

**The dashboards and their typical uses are listed below;**

**Malware Activity by Destination:** Look for Spikes or dips to spot any unusual activity based on previous history

**Malware Relationships:** Use this to discover relationships between infected hosts, or signatures

**Malware Detailed Activity:** Use this panel to understand the specifics of every malware event over a time span.

**Typical Sources of Information**:

*   Centralised Anti-Virus Server, or client endpoint logs.

**Required Fields**

|Splunk Field Name|Description|Example|
|--- |--- |--- |
|dest|The System that has been infected|win_dc_01|
|signature|The name given to the malicious software|A97M.Poison, AvA.600|
|action|Indicates the action taken by the Anti-Virus Engine|"blocked", "quarantined"|

Extract more fields for Malware Events or look at the tutorials page to find out how.


**Host Intrusion Tab**
----------------------

The host intrusion tab is designed to give you information from endpoint detection and response systems, or intelligent anti-virus solutions.

These types of systems can often alert you to (and sometimes block), attackers currently attempting to execute exploits either locally or remotely.

**The dashboards are their typical uses are listed below;**

**Host Intrusion Activity by Host:** Look for Spikes or dips to spot any unusual activity based on previous history

**Intrusion Detection Relationships:** Discover relationships between attack signatures and the hosts they are executed or attempted on

**Detailed Intrusion Detection Activity:** Use this panel to understand the specifics of every host intrusion attempt over a time span

**Typical Sources of Information:**

*   Anti-Virus Solutions with endpoint protection (Symantec, Sophos etc)
*   Endpoint Detection and Response (EDR) solutions (Carbon Black, CrowdStrike, Cylance etc)

**Required Fields**

|Splunk Field Name|Description|Example|
|--- |--- |--- |
|src_ip|If present the IP address of the attacking host (not always available)|5.42.34.34|
|dest_port|If present the destination port on the targeted host (not always available)|23|
|dest|The device name (or IP address) that has been affected|Win-002-LP|
|severity|The severity applied by the vendor product|critical,high|
|signature|The name given to the attempted attack|Trojan Back Door Activity|
|action|Whether the action failed or succeeded|"success", "failure"|


Extract more fields for Endpoint IDS Events or look at the setup dashboard to find out how.

**Process Execution Tab**
-------------------------

Process Execution is front and centre when determining the source application of network traffic, or identifying unauthorised, malicious or masquerading processes.

Filter this dashboard looking for unusual process directories, parent processes or command line activities.

**The dashboards are their typical uses are listed below;**

**Process Activity by Process Name:** Look for spikes or dips to spot any unusual activity based on previous history

**Process Relationships:** Look for suspicious relationships between child processes, directories and command line arguments

**Detailed Process Tracking:** Use this panel to understand the specifics of every process execution over a time span

**Typical Sources of Information:**

*   Client Windows Security Log (WinEventLog:Security)
*   Client Sysmon Events

**Required Fields**

|Splunk Field Name|Description|Example|
|--- |--- |--- |
|parent_process|Parent Process that began the executing process (either process name or PID)|c:\Windows\System32\svchost.exe OR 2816|
|process_id|Process ID (PID) or the executing process|2817|
|process_name|The text name of the process that is running|PsExec.exe|
|command_line|Any arguments that have been passed to the process|c:\windows\sysWOW64\cmd.exe|

Extract more fields for Process Events or look at the tutorials page to find out how.


**Vulnerabilities Tab**
-----------------------

The vulnerabilities tab allows you to visualise outstanding vulnerabilities, so that you may prioritise patching efforts by hosts, criticality etc.

Use this information to understand if it is feasible for attackers to exploit machines with certain vulnerabilities, and correlate with attempted intrusion information to gain situational awareness.

**The dashboards are their typical uses are listed below;**

**Vulnerability Activity by Signature:** Look for Spikes or dips to spot new and emerging vulnerabilities

**Vulnerability Relationships:** Discover relationships hosts, vulnerabilities and criticality

**Detailed Vulnerability Tracking:** Use this panel to understand exactly what vulnerabilities exist for individual hosts

**Typical Sources of Information:**

*   Vulnerability Scanners (Nessus, Rapid7 etc)

**Required Fields**

|Splunk Field Name|Description|Example|
|--- |--- |--- |
|dest|The host name or IP address of the vulnerable host|WIN-002-LPT OR 10.10.10.10|
|signature|The name of the vulnerability|SMB LanMan Pipe Server browse listing|
|cvss|The common vulnerability score|6|
|cve|The unique ID given to each publically known vulnerability|CVE-2018-3847|
|mskb|Microsoft Knowledge Base Number (if present)|KB2919355|

Extract more fields for Vulnerability Events or look at the tutorials dashboard to find out how.


**Plug and Play Tab**
---------------------

The plug and tab allows you to visualise instances of removable hardware used on a specific computer.

Use this information to understand what devices may have been used previously.

**The dashboards are their typical uses are listed below;**

**PnP Activity by Dest:** Look for Spikes from new hosts not expected to use external devices

**PnP Relationships:** Discover relationships between hosts and device\_uuids

**Detailed PnP Activity:** Use this panel to understand all connectivity of external hardware and associated global UUIDs

**Typical Sources of Information:**

*   Windows Registry Keys
*   Windows Event Logs (if audited)
*   Endpoint Protection Technologies (Sophos, Symantec etc)

**Required Fields**

|Splunk Field Name|Description|Example|
|--- |--- |--- |
|dest|The host name or IP address of the host|WIN-002-LPT OR 10.10.10.10|
|user|The user that connected the device|fred.flintstone|
|device_id|The manufacturer name and model (if present)|USBSTOR\samsung_flash_8.07|
|device_uuid|The unique ID given to every removable device|123e4567-e89b-12d3-a456-426655440000|
|action|The action taken by the reporting host or software|Alert Only, Blocked|

Extract more fields for Plug and Play Events or look at the tutorials dashboard to find out how.



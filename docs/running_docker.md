**Running a Docker Demo**
=========================
If you want to test out the Security Monitoring for Splunk app, you can download and run the docker image.

Simply run; 

	docker run -d -p 8000:8000 --env SPLUNK_START_ARGS="--accept-license --no-prompt --seed-passwd password" derekking/smfs:latest

If you run into any syntax errors, make sure you delete and re-input the quotes, as they have converted to smart quotes. 


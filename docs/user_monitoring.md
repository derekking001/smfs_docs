**User Monitoring Dashboard**
=============================

The user monitoring dashboard aims to monitor user centric information. You can click on any visualisation to drill into the raw events. Any table cells highlighted in red are fields that have not been extracted. Use the app setup dashboard to make any adjustments to you may need to fields.


**System Access Tab**
---------------------

The system access tab is designed to give you information about who is accessing what systems and when. If you are monitoring windows data, make sure you have the [Windows Technology Add-on](https://splunkbase.splunk.com/app/742/) installed on your indexer (and search head if you have a distributed environment)

Attackers and their malicious software must run as an authorised user on your network. Closely monitor access patterns to discover unusal activity.

Use the "Show and Hide Filters" button to have your dashboard react to information you care about.

**The dashboards and their typical uses are listed below;**

**System Access by action:** Look for Spikes or dips to spot any unusual activity based on previous history

**System Access Relationships:** Look for suspicious relationships between users and the resources you expect them to logon to

**System Access Detailed Activity:** Use this panel to understand the specifics of every system access over a time span.

**Windows Audit Policy**

This tab relies on windows EventCode 4624 (logon) and 4625 (logoff) being present in windows security logs. The "Account Logon" policy object must be configed for "success" and "failure".  
See [Microsoft security best practice guide](https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/security-best-practices/audit-policy-recommendations) for more information.

**Typical Sources of Information**:

*   Windows Security Eventlog from Domain Controllers and Endpoints.

**Required Fields**

|Splunk Field Name|Description|Example|
|--- |--- |--- |
|src_ip|Source IP address|192.168.1.200|
|dest|The System that has been accessed|win_dc_01|
|src_user|In privilege escalation events, the unprivileged user|wilma|
|user|The user name that made the access|wilma-admin|
|action|Indicates if access was granted or not|"success", "failure"|
|src_ip|The source ip adress of the logon event|192.168.10.10|
|logon_type|Numeric field found in Windows Authentication Events|3|

Extract more fields for Windows Security Events or look at the tutorials page to find out how.


**Authorisation Tab**
---------------------

The Authorisation tab is designed to give you information about authorisation changes, such as account lockouts, password resets and changes to privileges. Make sure you have the [Splunk Technology Add-on](https://splunkbase.splunk.com/app/742/) installed on your indexer (and search head if you have a distributed environment)

A tactic often used in breaches is to enable previously disabled accounts, reset passwords, add privilege to existing, or new create accounts and almost immediately delete them after use. Ensure this type of behaviour on your network has been authorised and understood.

**Windows Audit Policy**

This tab relies on windows EventCode=4720,4722,4725,4726,4738,4740 and 4767. To ensure you see these events, make sure Auditing for account management is configured for "success" and "failure". See [Windows Auditing Recomendations](https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/security-best-practices/audit-policy-recommendations) for further information.

**The dashboards are their typical uses are listed below;**

**Authorisation Activity by Action:** Look for Spikes or dips to spot any unusual activity based on previous history

**Authorisation Relationships:** Look for suspicious activity based on the executing user, host and change type

**Detailed Authorisation Tracking:** Use this panel to understand the specifics of every authorisation change over a time span

**Typical Sources of Information:**

*   Windows Security Logs
*   Windows System Logs

**Required Fields**

|Splunk Field Name|Description|Example|
|--- |--- |--- |
|user|The user account that has been affected|fred.flintstone|
|src_user|The admin user making the change|administrator@goingmad.com|
|signature|A Text description of the change event|A New User Account was Created|
|action|Whether the action failed or succeeded|"success", "failure"|


Extract more fields for Windows Security Events or look at the tutorials page to find out how.

**Group Changes Tab**
---------------------

Having situational awareness of changes (temporary or otherwise) to group permissions allows you spot operational policy issues, and in certain instances malicious activity.

Filter on time,initiating user, and host to discover the actions taken by individuals.

**Windows Audit Policy**

This tab relies on windows EventCode=4731,4727,4754,4735,4737,4755,4734,4730,4758,4732,4728,4756,4733,4729,4757,4723,4724. To ensure you see these events, make sure Auditing for account management is configured for "success" and "failure". See [Windows Auditing Recomendations](https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/security-best-practices/audit-policy-recommendations) for further information.

**The dashboards are their typical uses are listed below;**

**Group Changes By Signature:** Look for any changes made by unprivileged users using the initiating user drop down

**Group Change Relationships:** Discover relationships between users, hosts and change activity

**Detailed Groupd Changes Activity:** Use the dashboard filters to understand every change to group membership over time

**Typical Sources of Information:**

*   Windows Domain Controllers
*   Windows Member Servers

**Required Fields**

|Splunk Field Name|Description|Example|
|--- |--- |--- |
|src_user|The unprivileged account escalating privilege|fred.flintstone|
|action|The action taken or outcome|created, modified, deleted|
|signature|The practical change that occurred|Password Set, A Security-enabled global group was created|
|user|The user making the action, or new privileged account|fred_admin|

Extract more fields for Windows Security Events or look at the tutorials page to find out how.


